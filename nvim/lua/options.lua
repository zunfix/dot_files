local opt = vim.opt

opt.clipboard = "unnamedplus"
opt.cmdheight = 1
opt.colorcolumn = "80"
opt.cursorline = true
opt.guicursor = ""
opt.ignorecase = true
opt.laststatus = 3
opt.list = true
opt.mouse = ""
opt.number = true
opt.pumheight = 10
opt.relativenumber = true
opt.scrolloff = 8
opt.shiftwidth = 4
opt.showmode = false
opt.showtabline = 0
opt.sidescrolloff = 8
opt.smartcase = true
opt.smartindent = true
opt.spell = true
opt.splitbelow = true
opt.splitright = true
opt.tabstop = 4
opt.termguicolors = true
opt.undofile = true
opt.wrap = false
opt.writebackup = false

-- Decrease update time
opt.updatetime = 250
opt.timeoutlen = 300


-- Open file at previous cursor position
vim.cmd([[
  if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
  endif
]])

-- Remove trailing spaces on save
vim.api.nvim_create_autocmd(
  "BufWritePre",
  { pattern = { "*" }, command = ":%s/\\s\\+$//e"}
)

-- Highlight on yank
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})
