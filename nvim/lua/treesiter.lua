require('nvim-treesitter.configs').setup {
  ensure_installed = { 'c', 'cpp', 'go', 'lua', 'python', 'rust', 'vimdoc', 'vim', 'bash', 'cmake', },

  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },

  sync_install = false,
  auto_install = false,
  indent = { enable = true },
  ignore_install = {},
  modules = {},
}
