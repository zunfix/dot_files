local opts = { noremap = true, silent = true}
local keymap = vim.keymap.set

vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
vim.keymap.set({ 'n', 'v' }, '<space>', '<nop>', opts)

-- Better window navigation
keymap("n", "<c-h>", "<c-w>h", opts)
keymap("n", "<c-j>", "<c-w>j", opts)
keymap("n", "<c-k>", "<c-w>k", opts)
keymap("n", "<c-l>", "<c-w>l", opts)
keymap("n", "<c-r>", "<c-w>r", opts)

-- Diagnostic navigation
keymap('n', '[d', vim.diagnostic.goto_prev, opts)
keymap('n', ']d', vim.diagnostic.goto_next, opts)
keymap('n', '<leader>e', vim.diagnostic.open_float, opts)

keymap("n", "<", "^", opts)
keymap("n", ">", "$", opts)

-- Resize with arrows
keymap("n", "<a-j>", ":resize +2<cr>", opts)
keymap("n", "<a-k>", ":resize -2<cr>", opts)
keymap("n", "<a-h>", ":vertical resize -2<cr>", opts)
keymap("n", "<a-l>", ":vertical resize +2<cr>", opts)

-- Visual Block --
-- Move text up and down
keymap("x", "J", ":move '>+1<cr>gv-gv", opts)
keymap("x", "K", ":move '<-2<cr>gv-gv", opts)
keymap("x", "H", "<gv", opts)
keymap("x", "L", ">gv", opts)

-- Replace spaces with new lines
keymap("n", "<leader><cr>", ":s/ /\\r/g <cr> :noh <cr> :set nospell <cr>", opts)

-- Turn off spellcheck
keymap("n", "<leader>s", ":set nospell <cr>", opts)
-- Turn on spellcheck
keymap("n", "<leader>S", ":set nospell <cr>", opts)

-- Easy way to repeat macros
keymap("n", "<leader>.", "@@", opts)
